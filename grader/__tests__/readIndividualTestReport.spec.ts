import { describe, it, expect, vi } from 'vitest'
import { promises as fs } from 'fs'
vi.mock('fs')

import { readFullTestReport, readVersionReport } from '../readIndividualTestReport.mjs'
import { FullTestReport, GradedVersionReport } from '../../reportSchema'

describe('readVersionReport', () => {
  describe('when the version is higher than the current one', () => {
    it('returns future report', async () => {
      vi.mocked(fs).readFile.mockRejectedValueOnce(new Error())
  
      const result = await readVersionReport(4, 3, 8)
      expect(result.status).toBe('future')
      expect(result.grade.maximum).toBe(1.25)
    })
  
    it('ignores any file', async () => {
      vi.mocked(fs).readFile.mockRejectedValueOnce(new Error())
  
      const result = await readVersionReport(4, 3, 8)
      expect(result.status).toBe('future')
      expect(result.grade.maximum).toBe(1.25)
  
      expect(fs.readFile).toHaveBeenCalledTimes(0)
    })
  })

  const scenario = [
    { description: 'the current one', version: 3, status: 'current' },
    { description: 'lower than the current one', version: 2, status: 'locked' },
  ]
  for(let { description, version, status } of scenario) {
    describe('when the version is ' + description, () => {
      it('attemps the to load the right file', async () => {
        vi.mocked(fs).readFile.mockRejectedValueOnce(new Error())
    
        await readVersionReport(version, 3, 8)

        expect(fs.readFile).toHaveBeenCalledWith(`tests/version${version}.txt`)
      })

      it('returns a report with the right status and maximum when the file is unavailable', async () => {
        vi.mocked(fs).readFile.mockRejectedValueOnce(new Error())
    
        const result = await readVersionReport(version, 3, 8)
        expect(result.status).toBe(status)
        expect(result.grade.maximum).toBe(1.25)
      })
      
      it('returns a report with 0 as everything when the file is unavailable', async () => {
        vi.mocked(fs).readFile.mockRejectedValueOnce(new Error())
    
        const result = await readVersionReport(version, 3, 8)
        expect(result.status).toBe(status)
        
        const gradedReport = result as GradedVersionReport
        expect(gradedReport.success).toBe(0)
        expect(gradedReport.total).toBe(0)
        expect(gradedReport.rate).toBe(0)
        expect(gradedReport.grade.value).toBe(0)
      })
      
      it('returns a report with an error when the file is unavailable', async () => {
        vi.mocked(fs).readFile.mockRejectedValueOnce(new Error())
    
        const result = await readVersionReport(version, 3, 8)
        expect(result.status).toBe(status)
        
        const gradedReport = result as GradedVersionReport
        expect(gradedReport.error).toBe("Le résultat des tests n'est pas disponnible")
      })
      
      it('returns a report with 0 as everything when the file does not contain a correct Total', async () => {
        vi.mocked(fs).readFile.mockResolvedValueOnce("Succès: 12\nTotale: 14")
    
        const result = await readVersionReport(version, 3, 8)
        expect(result.status).toBe(status)
        expect(result.grade.maximum).toBe(1.25)

        const gradedReport = result as GradedVersionReport
        expect(gradedReport.success).toBe(0)
        expect(gradedReport.total).toBe(0)
        expect(gradedReport.rate).toBe(0)
        expect(gradedReport.grade.value).toBe(0)

        expect(gradedReport.error).toBe("Le résultat des tests n'est pas formé correctement")
      })
      
      it('returns a report with 0 as everything when the file does not contain a correct Succès', async () => {
        vi.mocked(fs).readFile.mockResolvedValueOnce("Success: 12\nTotal: 14")
    
        const result = await readVersionReport(version, 3, 8)
        expect(result.status).toBe(status)
        expect(result.grade.maximum).toBe(1.25)

        const gradedReport = result as GradedVersionReport
        expect(gradedReport.success).toBe(0)
        expect(gradedReport.total).toBe(0)
        expect(gradedReport.rate).toBe(0)
        expect(gradedReport.grade.value).toBe(0)

        expect(gradedReport.error).toBe("Le résultat des tests n'est pas formé correctement")
      })

      it('returns a report with the success and total when the file contains a correct Succès', async () => {
        vi.mocked(fs).readFile.mockResolvedValueOnce("Succès: 8\nTotal: 10")
    
        const result = await readVersionReport(version, 3, 8)
        expect(result.status).toBe(status)
        expect(result.grade.maximum).toBe(1.25)

        const gradedReport = result as GradedVersionReport
        expect(gradedReport.success).toBe(8)
        expect(gradedReport.total).toBe(10)
        expect(gradedReport.error).toBe(undefined)
      })

      it('returns a report with the rate between success and total when the file contains a correct Succès', async () => {
        vi.mocked(fs).readFile.mockResolvedValueOnce("Succès: 8\nTotal: 10")
    
        const result = await readVersionReport(version, 3, 8)
        expect(result.status).toBe(status)
        expect(result.grade.maximum).toBe(1.25)

        const gradedReport = result as GradedVersionReport
        expect(gradedReport.rate).toBe(0.8)
      })

      it('returns a report with a grade as the rate times the max grade when the file contains a correct Succès', async () => {
        vi.mocked(fs).readFile.mockResolvedValueOnce("Succès: 8\nTotal: 10")
    
        const result = await readVersionReport(version, 3, 8)
        expect(result.status).toBe(status)
        expect(result.grade.maximum).toBe(1.25)

        const gradedReport = result as GradedVersionReport
        expect(gradedReport.grade.value).toBe(1)
      })

      it('returns a correct report with a mishapen file when the file contains a correct Succès', async () => {
        vi.mocked(fs).readFile.mockResolvedValueOnce(`
  Succès  :   8   
Caramba  
  Total  : 10   
Parasite: Value
        `)
    
        const result = await readVersionReport(version, 3, 8)
        expect(result.status).toBe(status)
        expect(result.grade.maximum).toBe(1.25)

        const gradedReport = result as GradedVersionReport
        expect(gradedReport.success).toBe(8)
        expect(gradedReport.total).toBe(10)
        expect(gradedReport.rate).toBe(0.8)
        expect(gradedReport.grade.value).toBe(1)
        expect(gradedReport.error).toBe(undefined)
      })

      it('acts as 0 rate when the total is 0', async () => {
        vi.mocked(fs).readFile.mockResolvedValueOnce(`
  Succès  :   8   
Caramba  
  Total  : 0   
Parasite: Value
        `)
    
        const result = await readVersionReport(version, 3, 8)
        expect(result.status).toBe(status)
        expect(result.grade.maximum).toBe(1.25)

        const gradedReport = result as GradedVersionReport
        expect(gradedReport.success).toBe(8)
        expect(gradedReport.total).toBe(0)
        expect(gradedReport.rate).toBe(0)
        expect(gradedReport.grade.value).toBe(0)
        expect(gradedReport.error).toBe("Les tests n'ont pas pu s'exécuter correctement")
      })

      it('returns a 0 in everything rate when the total is NaN', async () => {
        vi.mocked(fs).readFile.mockResolvedValueOnce(`
  Succès  :   8   
Caramba  
  Total  : Not A Number   
Parasite: Value
        `)
    
        const result = await readVersionReport(version, 3, 8)
        expect(result.status).toBe(status)
        expect(result.grade.maximum).toBe(1.25)

        const gradedReport = result as GradedVersionReport
        expect(gradedReport.success).toBe(0)
        expect(gradedReport.total).toBe(0)
        expect(gradedReport.rate).toBe(0)
        expect(gradedReport.grade.value).toBe(0)
        expect(gradedReport.error).toBe("Le résultat des tests n'est pas formé correctement")
      })

      it('returns a 0 in everything rate when the success is NaN', async () => {
        vi.mocked(fs).readFile.mockResolvedValueOnce(`
  Succès  :   Not an integer   
Caramba  
  Total  : 8   
Parasite: Value
        `)
    
        const result = await readVersionReport(version, 3, 8)
        expect(result.status).toBe(status)
        expect(result.grade.maximum).toBe(1.25)

        const gradedReport = result as GradedVersionReport
        expect(gradedReport.success).toBe(0)
        expect(gradedReport.total).toBe(0)
        expect(gradedReport.rate).toBe(0)
        expect(gradedReport.grade.value).toBe(0)
        expect(gradedReport.error).toBe("Le résultat des tests n'est pas formé correctement")
      })
    })
  }
})


describe('readFullTestReport', () => {
  it('attemps the to load the right file', async () => {
    vi.mocked(fs).readFile.mockRejectedValueOnce(new Error())

    await readFullTestReport()

    expect(fs.readFile).toHaveBeenCalledWith(`tests/full.txt`)
  })

  it('returns a report with the maximum when the file is unavailable', async () => {
    vi.mocked(fs).readFile.mockRejectedValueOnce(new Error())

    const result = await readFullTestReport()
    expect(result.grade.maximum).toBe(10)
  })
  
  it('returns a report with 0 as everything when the file is unavailable', async () => {
    vi.mocked(fs).readFile.mockRejectedValueOnce(new Error())

    const result = await readFullTestReport()
    expect(result.success).toBe(0)
    expect(result.total).toBe(0)
    expect(result.rate).toBe(0)
    expect(result.grade.value).toBe(0)
  })
      
  it('returns a report with an error when the file is unavailable', async () => {
    vi.mocked(fs).readFile.mockRejectedValueOnce(new Error())

    const result = await readFullTestReport()
    expect(result.error).toBe("Le résultat des tests n'est pas disponnible")
  })
  
  it('returns a report with 0 as everything when the file does not contain a correct Total', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce("Succès: 12\nTotale: 14")

    const result = await readFullTestReport()
    expect(result.grade.maximum).toBe(10)
    expect(result.success).toBe(0)
    expect(result.total).toBe(0)
    expect(result.rate).toBe(0)
    expect(result.grade.value).toBe(0)

    expect(result.error).toBe("Le résultat des tests n'est pas formé correctement")
  })
  
  it('returns a report with 0 as everything when the file does not contain a correct Succès', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce("Success: 12\nTotal: 14")

    const result = await readFullTestReport()
    expect(result.grade.maximum).toBe(10)
    expect(result.success).toBe(0)
    expect(result.total).toBe(0)
    expect(result.rate).toBe(0)
    expect(result.grade.value).toBe(0)

    expect(result.error).toBe("Le résultat des tests n'est pas formé correctement")
  })

  it('returns a report with the success and total when the file contains a correct Succès', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce("Succès: 8\nTotal: 10")

    const result = await readFullTestReport()
    expect(result.grade.maximum).toBe(10)
    expect(result.success).toBe(8)
    expect(result.total).toBe(10)
    expect(result.error).toBe(undefined)
  })

  it('returns a report with the rate between success and total when the file contains a correct Succès', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce("Succès: 8\nTotal: 10")

    const result = await readFullTestReport()
    expect(result.grade.maximum).toBe(10)
    expect(result.rate).toBe(0.8)
  })

  it('returns a report with a grade as the rate times the max grade when the file contains a correct Succès', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce("Succès: 8\nTotal: 10")

    const result = await readFullTestReport()
    expect(result.grade.maximum).toBe(10)
    expect(result.grade.value).toBe(8)
  })

  it('returns a correct report with a mishapen file when the file contains a correct Succès', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce(`
Succès  :   8   
Caramba  
Total  : 10   
Parasite: Value
    `)

    const result = await readFullTestReport()
    expect(result.grade.maximum).toBe(10)
    expect(result.success).toBe(8)
    expect(result.total).toBe(10)
    expect(result.rate).toBe(0.8)
    expect(result.grade.value).toBe(8)
    expect(result.error).toBe(undefined)
  })

  it('acts as 0 rate when the total is 0', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce(`
Succès  :   8   
Caramba  
Total  : 0   
Parasite: Value
    `)

    const result = await readFullTestReport()
    expect(result.grade.maximum).toBe(10)
    expect(result.success).toBe(8)
    expect(result.total).toBe(0)
    expect(result.rate).toBe(0)
    expect(result.grade.value).toBe(0)
    expect(result.error).toBe("Les tests n'ont pas pu s'exécuter correctement")
  })

  it('acts ignores the file when the total is NaN', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce(`
Succès  :   8   
Caramba  
Total  : Coucou   
Parasite: Value
    `)

    const result = await readFullTestReport()
    expect(result.grade.maximum).toBe(10)
    expect(result.success).toBe(0)
    expect(result.total).toBe(0)
    expect(result.rate).toBe(0)
    expect(result.grade.value).toBe(0)
    expect(result.error).toBe("Le résultat des tests n'est pas formé correctement")
  })

  it('acts ignores the file when the success is NaN', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce(`
Succès  :   Plop   
Caramba  
Total  : 10   
Parasite: Value
    `)

    const result = await readFullTestReport()
    expect(result.grade.maximum).toBe(10)
    expect(result.success).toBe(0)
    expect(result.total).toBe(0)
    expect(result.rate).toBe(0)
    expect(result.grade.value).toBe(0)
    expect(result.error).toBe("Le résultat des tests n'est pas formé correctement")
  })
})
