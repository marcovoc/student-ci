import { createServer, type ViteDevServer } from 'vite'
import { promises as fs } from 'fs'
import { resolve } from 'path'
import * as url from 'url'
import type { Report } from '../reportSchema.js';

export async function writePage(branch: string, report: Report) {
  const date = new Date().toLocaleString('fr-FR')
  const html = await generateHtml(branch, date, report)
  await fs.writeFile('index.html', html)
}


async function generateHtml(branch: string, date:string, report: Report) {
  const vite = await createServer({
    server: { 
      middlewareMode: true,
      hmr: false
    },
    appType: 'custom'
  })

  try {
    return requestHtml(vite, branch, date, report)
  } catch (e) {
    if (e  instanceof Error) {
      vite.ssrFixStacktrace(e)
    }
    throw e
  } finally {
    await vite.close()
  }
}


async function requestHtml(vite: ViteDevServer, branch: string, date:string, report: Report) {
  const template = await getTemplate(vite)

  /**/
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const serverModule = await import('./ssr/main-server.mjs')
  const appHtml = await serverModule.render({ branch, date, report })
  return template.replace(`<!--ssr-outlet-->`, appHtml)
}

async function getTemplate(vite: ViteDevServer) {
  const __dirname = url.fileURLToPath(new URL('.', import.meta.url))
  const template = await fs.readFile(
    resolve(__dirname, 'app/index.html'),
    'utf-8'
  )

  return removeScripts(await vite.transformIndexHtml("index.html", template))
}

function removeScripts(content: string) {
  return content.replace(/<script.*?<\/script>/g, '')
}
