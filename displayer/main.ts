import { createApp } from 'vue'
import App from './App.vue'

import './assets/main.css'

const gradeReport = {
  branch: "prod",
  date: "13/09/2022",
  report: {
    quality: {
      rate: 0.61,
      criteria: {
        'Nommage variables': 0.9,
        'Usage adéquat des fonctionnalités du langage': 0.8,
        'Simplicité des fonctions': 0.7,
        'Simplicité des classes': 0.6,
        'Pas de dupplication de code': 0.5,
        'Séparation de responsabilités': 0.4,
        'Encapsulation': 0.3,
        'Couverture de tests': 0.7
      },
      penalty: .19,
    },
    tests: {
      versions: {
        'Version 1': {
          total: 5,
          success: 4,
          rate: 0.8,
          grade: {
            value: 1.14,
            maximum: 1.43
          },
          status: 'locked'
        },
        'Version 2': {
          total: 4,
          success: 2,
          rate: 0.5,
          grade: {
            value: 0.71,
            maximum: 1.43
          },
          status: 'locked'
        },
        'Version 3': {
          total: 10,
          success: 8,
          rate: 0.8,
          grade: {
            value: 1.14,
            maximum: 1.43
          },
          status: 'current'
        },
        'Version 4': {
          status: 'future',
          grade: {
            maximum: 1.43
          }
        },
        'Version 5': {
          status: 'future',
          grade: {
            maximum: 1.43
          }
        },
        'Version 6': {
          status: 'future',
          grade: {
            maximum: 1.43
          }
        },
        'Version 7': {
          status: 'future',
          grade: {
            maximum: 1.43
          }
        }
      },
      full: {
        total: 20,
        success: 18,
        rate: 0.9,
        grade: {
          value: 9,
          maximum: 10
        }
      }
    },
    subTotal: {
      value: 12,
      maximum: 14.29
    },
    total: {
      value: 9.67,
      maximum: 14.29
    }
  }
}

createApp(App, gradeReport).mount('#app')
