import { readCodeCoverageRate } from './readCodeCoverageRate.mjs'
import { readQualityReport } from './readQualityReport.mjs'
import type { QualityReport } from '../reportSchema.js'

async function computeQualityCriterias() {
  const criterias = await readQualityReport()
  criterias['Couverture de tests'] = await readCodeCoverageRate()
  return criterias
}

function computeAverage(numbers: number[]) {
  const sum = numbers.reduce((previous, current) => previous + current)
  return sum * 1.0 / numbers.length
}

export async function computeQualityReport(): Promise<QualityReport> {
  const criteria = await computeQualityCriterias()
  const rate = computeAverage(Object.values(criteria))
  const penalty = 0.5 - rate / 2.0

  return { criteria, rate, penalty }
}
