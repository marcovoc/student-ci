import type { QualityCriterias } from "../reportSchema.js"
import { promises as fs } from 'fs'
import { parseKeyValuePairLines } from "./parseKeyValuePairLines.mjs"

export async function readQualityReport(): Promise<QualityCriterias> {
  const defaultCriterias = {
    'Nommage variables': 1,
    'Usage adéquat des fonctionnalités du langage': 1,
    'Simplicité des fonctions': 1,
    'Simplicité des classes': 1,
    'Pas de dupplication de code': 1,
    'Séparation de responsabilités': 1,
    'Encapsulation': 1,
  }
  try {
    const buffer = await readQualityFile()
    if (!buffer) {
      return defaultCriterias
    }

    return parseQualityContent(buffer)
  } catch (e) {
    console.warn("Cannot parse quality file, defaulting to default. Cause :" + e)
    return defaultCriterias
  }
}

async function readQualityFile() {
  try {
    const buffer = await fs.readFile('quality.md')
    return buffer.toString()
  } catch {
    console.warn("No quality file, defaulting to default")
    return null
  }
}

function parseQualityContent(report: String): QualityCriterias {
  const lines = report.split('\n')
      .filter(line => line[0] === '*')
      .map(line => line.substring(1).trim())

  return parseKeyValuePairLines(lines)
}
