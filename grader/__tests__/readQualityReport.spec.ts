import { describe, it, expect, vi } from 'vitest'

import { promises as fs } from 'fs'
vi.mock('fs')

import readQualityReport from '../readQualityReport.mjs'

describe('readQualityReport', () => {
  const defaultValue = {
    'Nommage variables': 1,
    'Usage adéquat des fonctionnalités du langage': 1,
    'Simplicité des fonctions': 1,
    'Simplicité des classes': 1,
    'Pas de dupplication de code': 1,
    'Séparation de responsabilités': 1,
    'Encapsulation': 1,
  }

  it('returns parsed content when file contains float to', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce(`* Nommage variables: 0.9
* Usage adéquat des fonctionnalités du langage: 0.8
* Simplicité des fonctions: 0.7
* Simplicité des classes: 0.6
* Pas de dupplication de code: 0.5
* Séparation de responsabilités: 0.4
* Encapsulation: 0.3`)

    const result = await readQualityReport()
    expect(result).toStrictEqual({
      'Nommage variables': 0.9,
      'Usage adéquat des fonctionnalités du langage': 0.8,
      'Simplicité des fonctions': 0.7,
      'Simplicité des classes': 0.6,
      'Pas de dupplication de code': 0.5,
      'Séparation de responsabilités': 0.4,
      'Encapsulation': 0.3,
    })

    expect(fs.readFile).toHaveBeenCalledWith('quality.md')
  })

  it('returns default content when file does not exist', async () => {
    vi.mocked(fs).readFile.mockRejectedValueOnce(new Error())

    const result = await readQualityReport()
    expect(result).toStrictEqual(defaultValue)
    expect(fs.readFile).toHaveBeenCalledWith('quality.md')
  })

  it('returns default content when file is empty', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce("")

    const result = await readQualityReport()
    expect(result).toStrictEqual(defaultValue)
    expect(fs.readFile).toHaveBeenCalledWith('quality.md')
  })

  it('ignores lines that do not start with a star', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce(`* Nommage variables: 0.9
* Usage adéquat des fonctionnalités du langage: 0.8
* Simplicité des fonctions: 0.7
Simplicité des classes: 0.6
* Pas de dupplication de code: 0.5
* Séparation de responsabilités: 0.4
* Encapsulation: 0.3
`)

    const result = await readQualityReport()
    expect(result).toStrictEqual({
      'Nommage variables': 0.9,
      'Usage adéquat des fonctionnalités du langage': 0.8,
      'Simplicité des fonctions': 0.7,
      // 'Simplicité des classes': 0.6,
      'Pas de dupplication de code': 0.5,
      'Séparation de responsabilités': 0.4,
      'Encapsulation': 0.3,
    })

    expect(fs.readFile).toHaveBeenCalledWith('quality.md')
  })

  it('trim lines after filtering with stars, and also around colons', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce(`* Nommage variables: 0.9
* Usage adéquat des fonctionnalités du langage:0.8
  * Simplicité des fonctions: 0.7  
*   Simplicité des classes: 0.6
* Pas de dupplication de code: 0.5
* Séparation de responsabilités   :   0.4
* Encapsulation: 0.3
`)

    const result = await readQualityReport()
    expect(result).toStrictEqual({
      'Nommage variables': 0.9,
      'Usage adéquat des fonctionnalités du langage': 0.8,
      // 'Simplicité des fonctions': 0.7,
      'Simplicité des classes': 0.6,
      'Pas de dupplication de code': 0.5,
      'Séparation de responsabilités': 0.4,
      'Encapsulation': 0.3,
    })

    expect(fs.readFile).toHaveBeenCalledWith('quality.md')
  })

  it('ignores lines where the value is not a valid number', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce(`* Nommage variables: 0.9
* Usage adéquat des fonctionnalités du langage: 0.8
* Simplicité des fonctions: 0.7  
* Simplicité des classes: 0.6
* Pas de dupplication de code: caramba
* Séparation de responsabilités: 
* Encapsulation: 0.3`)

    const result = await readQualityReport()
    expect(result).toStrictEqual({
      'Nommage variables': 0.9,
      'Usage adéquat des fonctionnalités du langage': 0.8,
      'Simplicité des fonctions': 0.7,
      'Simplicité des classes': 0.6,
      // 'Pas de dupplication de code': 0.5,
      // 'Séparation de responsabilités': 0.4,
      'Encapsulation': 0.3,
    })

    expect(fs.readFile).toHaveBeenCalledWith('quality.md')
  })

  it('ignores lines there is no colon', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce(`* Nommage variables: 0.9
* Usage adéquat des fonctionnalités du langage 0.8
* Simplicité des fonctions: 0.7  
* Simplicité des classes: 0.6
* Pas de dupplication de code: 0.5
* Séparation de responsabilités: 0.4
* Encapsulation: 0.3`)

    const result = await readQualityReport()
    expect(result).toStrictEqual({
      'Nommage variables': 0.9,
      // 'Usage adéquat des fonctionnalités du langage': 0.8,
      'Simplicité des fonctions': 0.7,
      'Simplicité des classes': 0.6,
      'Pas de dupplication de code': 0.5,
      'Séparation de responsabilités': 0.4,
      'Encapsulation': 0.3,
    })

    expect(fs.readFile).toHaveBeenCalledWith('quality.md')
  })
})
