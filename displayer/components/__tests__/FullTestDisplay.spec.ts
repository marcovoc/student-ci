import type { FullTestReport } from 'reportSchema'
import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import FullTestDisplay from '../FullTestDisplay.vue'

const report: FullTestReport = {
  total: 20,
  success: 18,
  rate: 0.9,
  grade: {
    value: 9,
    maximum: 10
  }
}

describe('FullTestDisplay', () => {
  it('displays a proper header', () => {
    const wrapper = mount(FullTestDisplay, { 
      props: { report }
    })
    const header = wrapper.get('h2')
    expect(header.text()).toBe('Tous les tests')
  })
  it('displays its rate', () => {
    const wrapper = mount(FullTestDisplay, { 
      props: { report }
    })
    const rate = wrapper.get('.rate')
    expect(rate.text()).toBe('90%')
  })
  it('displays its grade', () => {
    const wrapper = mount(FullTestDisplay, { 
      props: { report }
    })
    const gradeValue = wrapper.get('.grade-value')
    expect(gradeValue.text()).toBe('9')
    
    const gradeMaximum = wrapper.get('.grade-maximum')
    expect(gradeMaximum.text()).toBe('10')
  })
})
