import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { viteSingleFile } from "vite-plugin-singlefile"

console.log("Generating config")

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(), 
    viteSingleFile({
      inlinePattern: [
        "**/*.css"
      ]
    }), 
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  build: {
    outDir: 'grader/app'
  }
})
