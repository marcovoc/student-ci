# Qualité

* Nommage variables: 0.9
* Usage adéquat des fonctionnalités du langage: 0.8
* Simplicité des fonctions: 0.7
* Simplicité des classes: 0.6
* Pas de dupplication de code: 0.5
* Séparation de responsabilités: 0.4
* Encapsulation: 0.3
* Couverture de tests: 0
